﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using System.Net.Http;
using CoolParking.WebAPI.Errors;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private ParkingService parkingService;
        public VehiclesController(ParkingService service){ parkingService = service; }        

        // GET: api/vehicles
        [HttpGet]
        public IEnumerable<Vehicle> Get()
        {
            return parkingService.GetVehicles();
        }

        // GET: api/vehicles/FF-3333-FF
        [HttpGet("{id}", Name = "Get")]
        public ActionResult<Vehicle> Get(string id)
        {
            if (Vehicle.IsIdValid(id))
            {
                try
                {
                    return parkingService.GetVehicles().Single(x => x.Id == id);
                }
                catch(Exception)
                {
                    return StatusCode(404, new ErrorTemplate { type = "Argument exeption", title = "vehicle not found", status ="404", traceId = "" });
                }
            }
            else return StatusCode(400, new ErrorTemplate { type = "Argument exeption", title = "id is invalid", status = "400", traceId = "" });

            //return StatusCode(400, "id is invalid");
        }

        //POST: api/vehicles
        [HttpPost]
        public ActionResult<Vehicle> Post(Vehicle vehicle)
        {
            try
            {
                parkingService.AddVehicle(vehicle);
                return StatusCode(201);
            }
            catch (Exception)
            {
                return StatusCode(400, new ErrorTemplate { type = "Invalid body schema", title = "body is invalid", status = "400", traceId = "" });
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            if (Vehicle.IsIdValid(id))
            {
                try
                {
                    parkingService.RemoveVehicle(id);

                    return StatusCode(204);
                }
                catch (Exception)
                {
                    return StatusCode(404, new ErrorTemplate { type = "Argument exeption", title = "vehicle not found", status = "404", traceId = "" });
                }
            }
            else return StatusCode(400, new ErrorTemplate { type = "Argument exeption", title = "id is invalid", status = "400", traceId = "" });
        }
    }
}
