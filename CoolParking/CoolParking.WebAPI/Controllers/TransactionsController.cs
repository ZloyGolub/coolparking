﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.WebAPI.Errors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private ParkingService parkingService;
        public TransactionsController(ParkingService service) { parkingService = service; }

        // GET: api/Transactions/last
        [HttpGet("last")]
        public TransactionInfo Get()
        {
            return parkingService.GetLastTrans();
        }

        // POST: api/Transactions/all
        [HttpGet("all")]
        public ActionResult<string> Post()
        {
            string transactions = "";
            try
            {
                TransactionInfo[] infos = parkingService.GetLastParkingTransactions();
                
                foreach (var item in infos)
                {
                    transactions += $"{item.transactionDate}: {item.sum} money withdrawn from vehicle with Id='{item.vehicleId}'.\n";
                }
            }
            catch (Exception)
            {
                return StatusCode(404, new ErrorTemplate { type = "File existing error", title = "Not Found", status = "404", traceId = "" });
            }
            return transactions;
        }


        // PUT: api/Transactions/topUpVehicle
        [HttpPut("topUpVehicle")]
        public ActionResult<Vehicle> Put(TransactionInfo info)
        {
            try
            {
                if (Vehicle.IsIdValid(info.vehicleId))
                {
                    parkingService.TopUpVehicle(info.vehicleId, info.sum);
                    return parkingService.GetVehicles().Single(x => x.Id == info.vehicleId);
                }
                else return StatusCode(400, new ErrorTemplate { type = "Argument exeption", title = "body is invalid", status = "404", traceId = "" });
            }
            catch (Exception)
            {
                return StatusCode(404, new ErrorTemplate { type = "Argument exeption", title = "vehicle not found", status = "404", traceId = "" });
            }
        }

    }
}
