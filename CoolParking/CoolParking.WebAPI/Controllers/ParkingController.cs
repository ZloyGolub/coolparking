﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private ParkingService parkingService;
        public ParkingController(ParkingService service) { parkingService = service; }

        [HttpGet("balance")]
        public decimal BalanceGet()
        {
            return parkingService.GetBalance();
        }

        [HttpGet("capacity")]
        public int CapacityGet()
        {
            return parkingService.GetCapacity();
        }

        [HttpGet("freePlaces")]
        public int FreePlacesGet()
        {
            return parkingService.GetFreePlaces();
        }
    }
}
