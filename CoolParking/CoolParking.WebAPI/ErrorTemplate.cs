﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Errors
{
    public struct ErrorTemplate
    {
        public string type { get; set; }
        public string title { get; set; }
        public string status { get; set; }
        public string traceId { get; set; }

    }
}
