﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer Timer;
        public double Interval { get; set; } = 5000;

        public event ElapsedEventHandler Elapsed;

        public void OnElapsed(object sender, ElapsedEventArgs e) => Elapsed?.Invoke(this, null);
        
        public void Dispose()
        {
            Timer.Dispose();
        }

        public void Start()
        {
            Timer.Start();
        }

        public void Stop()
        {
            Timer.Start();
        }

        public void SetTimer(double Interval)
        {
            this.Interval = Interval * 1000;
            Timer = new Timer(this.Interval);
            Timer.Interval = this.Interval;
            Timer.AutoReset = true;
            Timer.Enabled = true;
            Timer.Elapsed += OnElapsed;
        }

        public TimerService(double Interval)
        {            
            this.Interval = Interval * 1000;
            Timer = new Timer(this.Interval);
            Timer.Interval = this.Interval;
            Timer.AutoReset = true;
            Timer.Enabled = true;
            Timer.Elapsed += OnElapsed;
        }

        public TimerService()
        {
            Timer = new Timer();
            Timer.Elapsed += OnElapsed;
        }
    }
}