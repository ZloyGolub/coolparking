﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService, IDisposable
    {
        private ITimerService withdrawTimer;
        private ITimerService logTimer;
        private ILogService logService;

        public decimal parkingWithdrawSum = 0;

        public List<TransactionInfo> LocalTransaction = new List<TransactionInfo>();

        private readonly Parking _parking = Parking.GetParking();
        public void AddVehicle(Vehicle vehicle)
        {
            Parking parking = Parking.GetParking();

            if (vehicle.Id.Equals(parking.vehicles.FirstOrDefault(x=> x.Id.Equals(vehicle.Id))?.Id))
            {
                throw new ArgumentException();
            }
            else
            {
                parking.vehicles.Add(vehicle);
            }
            
        }

        public void Dispose()
        {
            _parking.Dispose();
        }

        public decimal GetBalance()
        {
            return _parking.ParkingBalance;
        }

        public int GetCapacity()
        {            
            return _parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return _parking.Capacity - _parking.vehicles.Count();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            ReadOnlyCollection<TransactionInfo> transactionInfos = new ReadOnlyCollection<TransactionInfo>(_parking.transactionInfos);
            TransactionInfo[] infosMass = new TransactionInfo[transactionInfos.Count];
            for (int i = 0; i < transactionInfos.Count; i++)
            {
                infosMass[i] = transactionInfos.ElementAt(i);
            }
            return infosMass;
        }

        public TransactionInfo GetLastTrans()
        {
            ReadOnlyCollection<TransactionInfo> transactionInfos = new ReadOnlyCollection<TransactionInfo>(_parking.transactionInfos);
            return transactionInfos.LastOrDefault();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehicles = new ReadOnlyCollection<Vehicle>(_parking.vehicles);
            return vehicles;
        }

        public string ReadFromLog()
        {
            throw new System.NotImplementedException();
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle vehicle = _parking.vehicles.FirstOrDefault(x => x.Id.Equals(vehicleId));
            if (vehicle != null) _parking.vehicles.Remove(vehicle);
            else throw new ArgumentException();
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle vehicle = _parking.vehicles.FirstOrDefault(x => x.Id.Equals(vehicleId));
            if (vehicle != null && sum >= 0)
            {
                vehicle.Balance += sum;
            }
            else throw new ArgumentException();
        }

        private void RegualrWithdraw(Object sender, ElapsedEventArgs e)
        {
            decimal withdrawSum = 0;
            foreach (var item in _parking.vehicles)
            {
                decimal Tariff = Settings.Tariffs[item.VehicleType];
                decimal withdraw = Tariff;
                if (item.Balance < Tariff)
                {
                    decimal difference = Tariff - item.Balance;
                    decimal Fine = difference * Settings.FineCoefficient;
                    withdraw = (Tariff - difference) + Fine;
                }
                item.Balance -= withdraw;

                TransactionInfo ts = new TransactionInfo() {
                    transactionDate = DateTime.Now,
                    vehicleId = item.Id,
                    sum = withdraw
                };

                _parking.transactionInfos.Add(ts);
                LocalTransaction.Add(ts);

                withdrawSum += withdraw;
                parkingWithdrawSum += withdrawSum;
            }
            _parking.ParkingBalance += withdrawSum;            
        }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.logService = logService;
            this.withdrawTimer = withdrawTimer;
            this.withdrawTimer.Elapsed += RegualrWithdraw;
            this.withdrawTimer.Start();

            this.logTimer = logTimer;
            this.logTimer.Elapsed += RegualrLog;
            this.logTimer.Start();
        }

        private void RegualrLog(Object sender, ElapsedEventArgs e)
        {
            string logString = "";
            foreach (var item in _parking.transactionInfos)
            {
                logString += item.transactionDate + "\t" + item.vehicleId + "\t" + item.sum + "\n";
            }
            logService.Write(logString);
            _parking.transactionInfos.Clear();
            LocalTransaction.Clear();
            parkingWithdrawSum = 0;
        }

        public ParkingService()
        {
            string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\transactions.log";
            this.logService = new LogService(_logFilePath);
            this.withdrawTimer = new TimerService(Settings.TimerOut); ;
            this.withdrawTimer.Elapsed += RegualrWithdraw;
            this.withdrawTimer.Start();

            this.logTimer = new TimerService(Settings.TimerLog);
            this.logTimer.Elapsed += RegualrLog;
            this.logTimer.Start();
        }
    }
}