﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        public decimal ParkingBalance { get; set; } = 0;
        public int Capacity { get; } = Settings.ParkingCapacity;
        public List<Vehicle> vehicles { get; set; }
        public List<TransactionInfo> transactionInfos = new List<TransactionInfo>();    
        private Parking()
        {
            vehicles = new List<Vehicle>();
        }
        private static Parking _instance;

        public static Parking GetParking()
        {
            if (_instance == null)
            {
                _instance = new Parking();
            }
            return _instance;
        }

        public void Dispose()
        {
            _instance = null;
        }
    }
}