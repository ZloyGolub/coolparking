﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {

        public string Id {get; }

        public VehicleType VehicleType { get; }

        public decimal Balance { get; set; } //TODO The Balance should be able to change only in the CoolParking.BL project.

        [JsonConstructor]
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (IsIdValid(id) && IsBalanceValid(balance))
            {
                Id = id;
                VehicleType = vehicleType;
                Balance = balance;
            }
            else throw new ArgumentException();
        }
        
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random rnd = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            char char1 = chars[rnd.Next(0, 26)];
            char char2 = chars[rnd.Next(0, 26)];
            int number1 = rnd.Next(0, 9);
            int number2 = rnd.Next(0, 9);
            int number3 = rnd.Next(0, 9);
            int number4 = rnd.Next(0, 9);
            char char3 = chars[rnd.Next(0, 26)];
            char char4 = chars[rnd.Next(0, 26)];
            string s = char1 + char2 + "-" + number1 + number2 + number3 + number4 + "-" + char3 + char4;
            return s;
        }

        public static bool IsIdValid(string plate)
        {
            if (Regex.IsMatch(plate, @"^[A-Z]{2}-\d{4}-[A-Z]{2}(?=\r?$)"))
                return true;
            else return false;
        }
        private bool IsBalanceValid(decimal balance)
        {
            if (balance > 0)
            {
                return true;
            }
            else return false;
        }
    }
}