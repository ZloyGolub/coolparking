﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace ConsoleClient
{
    class UserHttp
    {
        private static readonly HttpClient client = new HttpClient();


        private string[] Choise = {
                "1. Вывести на экран текущий баланс Парковки.",
                "2. Вывести на экран сумму заработанных денег за текущий период (до записи в лог).",
                "3. Вывести на экран количество свободных/занятых мест на парковке.",
                "4. Вывести на экран все Транзакции Парковки за текущий период (до записи в лог).",
                "5. Вывести историю транзакций (считав данные из файла Transactions.log).",
                "6. Вывести на экран список Тр. средств находящихся на Паркинге.",
                "7. Поставить Тр. средство на Паркинг.",
                "8. Забрать транспортное средство с Паркинга.",
                "9. Пополнить баланс конкретного Тр. средства."
            };

        public void ShowChoises()
        {
            foreach (var item in Choise)
            {
                Console.WriteLine(item);
            }
        }

        public async Task<bool> Dispatch()
        {
            try
            {
                string type = GetKey();
                switch (type)
                {
                    case "1":
                        {
                            HttpResponseMessage respone = await client.GetAsync("https://localhost:44345/api/parking/balance");
                            if (respone.IsSuccessStatusCode)
                            {
                                Console.WriteLine("Баланс парковки: " + respone.Content.ReadAsStringAsync().Result);
                            }
                            break;
                        }
                    case "2":
                        this.ShowPeriodMoneyAction();
                        break;
                    case "3":
                        {
                            HttpResponseMessage responeFree = await client.GetAsync("https://localhost:44345/api/parking/freePlaces");
                            HttpResponseMessage responeCapacity = await client.GetAsync("https://localhost:44345/api/parking/capacity");
                            if (responeFree.IsSuccessStatusCode && responeCapacity.IsSuccessStatusCode)
                            {

                                Console.WriteLine($"Свободные места: {responeFree.Content.ReadAsStringAsync().Result} Занятые места: {Convert.ToInt32(responeCapacity.Content.ReadAsStringAsync().Result) - Convert.ToInt32(responeFree.Content.ReadAsStringAsync().Result)}");
                            }
                            break;
                        }
                    case "4":
                        this.ShowPeriodTransactionsAction();
                        break;
                    case "5":
                        {
                            TransactionInfo[] transactionInfos;
                            HttpResponseMessage respone = await client.GetAsync("https://localhost:44345/api/transactions/all");

                            if (respone.IsSuccessStatusCode)
                            {
                                string body = respone.Content.ReadAsStringAsync().Result;
                                transactionInfos = JsonConvert.DeserializeObject<TransactionInfo[]>(body);

                                Console.WriteLine("Time    Id    Sum");
                                foreach (var item in transactionInfos)
                                {
                                    Console.WriteLine($"{item.transactionDate} {item.vehicleId} {item.sum}");
                                }
                            }
                            break;
                        }
                    case "6":
                        this.ShowVeiclesAction();
                        break;
                    case "7":
                        this.AddVehicleAction();
                        break;
                    case "8":
                        this.TakeVehicleAction();
                        break;
                    case "9":
                        this.AddCashAction();
                        break;
                    default:
                        Console.WriteLine("Incorect key");
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            return true;
        }

        private async void ShowPeriodTransactionsAction()
        {
            using HttpResponseMessage response = await client.GetAsync("https://localhost:44345/api/transactions/last");
            string body = response.Content.ReadAsStringAsync().Result;
            TransactionInfo[] transactions = JsonConvert.DeserializeObject<TransactionInfo[]>(body);

            Console.WriteLine("Время \t\t\tНомер \t\tСнято");
            foreach (var x in transactions)
            {
                Console.WriteLine($"{x.transactionDate}   {x.vehicleId}  {x.sum}");
            }
        }

        private string GetKey()
        {
            return Console.ReadLine();
        }

        private async void ShowVeiclesAction()
        {
            using HttpResponseMessage response = await client.GetAsync("https://localhost:44345/api/vehicles");
            string body = response.Content.ReadAsStringAsync().Result;
            var vehicles = JsonConvert.DeserializeObject<ReadOnlyCollection<Vehicle>>(body);

            Console.WriteLine("Номер \t\tТип \t\tБаланс");

            foreach (var item in vehicles)
            {
                Console.WriteLine(item.Id + "\t" + item.VehicleType + "\t" + item.Balance);
            }
        }

        private async void ShowPeriodMoneyAction()
        {
            try
            {
                using HttpResponseMessage response = await client.GetAsync("https://localhost:44345/api/transactions/last");
                string body = response.Content.ReadAsStringAsync().Result;
                TransactionInfo[] transactions = JsonConvert.DeserializeObject<TransactionInfo[]>(body);
                Console.WriteLine("Сума: " + transactions.Sum(x => x.sum));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private async void AddVehicleAction()
        {
            try
            {
                Console.WriteLine("Введите номер автомобиля: ");
                string Plate = Console.ReadLine();
                Console.WriteLine("\n Тип втомобиля" +
                "\n Легковой - 1" +
                "\n Грузовой - 2" +
                "\n Автобус  - 3" +
                "\n Мотоцикл - 4)");
                int CarType = Convert.ToInt32(Console.ReadLine());
                while (CarType < 1 || CarType > 4)
                {
                    Console.WriteLine("Введите коректный тип автомобиля: ");
                    CarType = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("Баланс: ");
                decimal cash = Convert.ToDecimal(Console.ReadLine());
                Vehicle vehicle = new Vehicle(Plate, (VehicleType)CarType, cash);
                var data = new StringContent(JsonConvert.SerializeObject(vehicle), Encoding.UTF8, "application/json");
                await client.PostAsync("https://localhost:44345/api/vehicles", data);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Некоректные данные номера");
            }
            catch (FormatException)
            {
                Console.WriteLine("Неверно выбран тип");
            }
        }

        private async void TakeVehicleAction()
        {
            try
            {
                Console.WriteLine("Введите номер автомобиля: ");
                string Plate = Console.ReadLine();
                await client.DeleteAsync($"https://localhost:44345/api/vehicles/{Plate}");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Такого автомобиля нет на паркинге");
            }
        }

        private async void AddCashAction()
        {
            try
            {
                Console.WriteLine("Введите номер автомобиля: ");
                string Plate = Console.ReadLine();
                Console.WriteLine("Введите суму пополнения: ");
                decimal Sum = Convert.ToDecimal(Console.ReadLine());
                var data = new StringContent($"{{ \"id\": \"{Plate}\", \"Sum\": {Sum} }}",
                Encoding.UTF8, "application/json");
                await client.PutAsync("https://localhost:44345/api/transactions/topUpVehicle", data);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Такого автомобиля нет на паркинге");
            }

        }

    }
}
