﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace ConsoleLook
{
    class UserMenu
    {
        private static string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\transactions.log";

        private static ITimerService withdrawTimer = new TimerService(Settings.TimerOut);
        private static ITimerService logTimer = new TimerService(Settings.TimerLog);
        private static ILogService logService = new LogService(_logFilePath);


        private ParkingService parkingService = new ParkingService(withdrawTimer, logTimer, logService);

        private string[] Choise = {
                "1. Вывести на экран текущий баланс Парковки.",
                "2. Вывести на экран сумму заработанных денег за текущий период (до записи в лог).",
                "3. Вывести на экран количество свободных/занятых мест на парковке.",
                "4. Вывести на экран все Транзакции Парковки за текущий период (до записи в лог).",
                "5. Вывести историю транзакций (считав данные из файла Transactions.log).",
                "6. Вывести на экран список Тр. средств находящихся на Паркинге.",
                "7. Поставить Тр. средство на Паркинг.",
                "8. Забрать транспортное средство с Паркинга.",
                "9. Пополнить баланс конкретного Тр. средства."
            };
        
        public void ShowChoises()
        {
            foreach (var item in Choise)
            {
                Console.WriteLine(item);
            }
        }

        public bool Dispatch()
        {
            try
            {
                string type = GetKey();
                switch (type)
                {
                    case "1":
                        Console.WriteLine("Баланс парковки: " + parkingService.GetBalance());
                        break;
                    case "2":
                        this.ShowPeriodMoneyAction();
                        break;
                    case "3":
                        Console.WriteLine($"Свободные места: {parkingService.GetFreePlaces()} Занятые места: { parkingService.GetCapacity() - parkingService.GetFreePlaces()}");
                        break;
                    case "4":
                        this.ShowPeriodTransactionsAction();
                        break;
                    case "5":
                        foreach (var item in parkingService.GetLastParkingTransactions())
                        {
                            Console.WriteLine(item);
                        }
                        break;
                    case "6":
                        this.ShowVeiclesAction();
                        break;
                    case "7":
                        this.AddVehicleAction();
                        break;
                    case "8":
                        this.TakeVehicleAction();
                        break;
                    case "9":
                        this.AddCashAction();
                        break;
                    default:
                        Console.WriteLine("Incorect key");
                        break;
                }
            } 
            catch (Exception e)
            {
                Console.WriteLine(e);

            }

            return true;
        }

        private void ShowPeriodTransactionsAction()
        {
            Console.WriteLine("Время \t\t\tНомер \t\tСнято");
            foreach (var item in parkingService.LocalTransaction)
            {
                Console.WriteLine(item.transactionDate + "\t" + item.vehicleId + "\t" + item.sum);
            }
        }

        private string GetKey()
        {
            return Console.ReadLine();
        }

        private void ShowVeiclesAction()
        {
            Console.WriteLine("Номер \t\tТип \t\tБаланс");

            foreach (var item in parkingService.GetVehicles())
            {
                Console.WriteLine(item.Id + "\t" + item.VehicleType + "\t" + item.Balance);
            }
        }

        private void ShowPeriodMoneyAction()
        {
            Console.WriteLine("Сума: " + parkingService.parkingWithdrawSum);

        }

        private void AddVehicleAction()
        {
            try
            {
                Console.WriteLine("Введите номер автомобиля: ");
                string Plate = Console.ReadLine();
                Console.WriteLine("\n Тип втомобиля" +
                "\n Легковой - 1" +
                "\n Грузовой - 2" +
                "\n Автобус  - 3" +
                "\n Мотоцикл - 4)");
                int CarType = Convert.ToInt32(Console.ReadLine());
                while (CarType < 1 || CarType > 4)
                {
                    Console.WriteLine("Введите коректный тип автомобиля: ");
                    CarType = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("Баланс: ");
                decimal cash = Convert.ToDecimal(Console.ReadLine());
                Vehicle vehicle = new Vehicle(Plate, (VehicleType)CarType, cash);
                parkingService.AddVehicle(vehicle);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Некоректные данные номера");
            }
            catch (FormatException)
            {
                Console.WriteLine("Неверно выбран тип");
            }
        }

        private void TakeVehicleAction()
        {
            try
            {
                Console.WriteLine("Введите номер автомобиля: ");
                string Plate = Console.ReadLine();
                parkingService.RemoveVehicle(Plate);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Такого автомобиля нет на паркинге");
            }
        }

        private void AddCashAction()
        {
            try
            {
                Console.WriteLine("Введите номер автомобиля: ");
                string Plate = Console.ReadLine();
                Console.WriteLine("Введите суму пополнения: ");
                decimal Sum = Convert.ToDecimal(Console.ReadLine());
                parkingService.TopUpVehicle(Plate,Sum);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Такого автомобиля нет на паркинге");
            }

        }

    }
}
